import { Injectable } from '@angular/core';
import { Http, Response, Headers, RequestOptions } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import 'rxjs/Rx';

@Injectable()
export class ItemsService {

        constructor(private http: Http){}

        getItems(){
            return this.http.get('https://start.avalancheonline.ru/api/v2/landing/orders/?token=ea685397-d24c-4416-86ff-71277c1061ef')
            .map(response => <Items[]>response.json().items)
        }
}
