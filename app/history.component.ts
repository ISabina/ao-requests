import { Component, OnInit, Input } from '@angular/core';
import { Response} from '@angular/http';
import { Injectable } from '@angular/core';
import { Http, Response, Headers, RequestOptions } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import { HistoryService} from './history.service';
import { History} from './history';


@Component({
    selector: 'history-app',
    templateUrl: `app/history.component.html`,
    styleUrls: [`app/app.component.css`],
    providers:    [ HistoryService ],
    inputs: ['orderId']



})

export class HistoryComponent {



    history: History [] ;
    comments: Comments [];

    constructor(private historyService: HistoryService){}


    ngOnInit() {
        this.historyService.getHistory(this.orderId)
        .subscribe( history => this.history = history);

        this.historyService.getComments(this.orderId)
        .subscribe( comments => this.comments = comments);
    }

    statuses = {
    "red": "diff-status-red",
    "yellow": "diff-status-yellow",
    "green": "diff-status-green"
  }



}