import { Component, OnInit, ViewChild } from '@angular/core';
import { Subscription} from 'rxjs/Subscription';
import { Injectable } from '@angular/core';
import { Http, Response, Headers, RequestOptions } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import { Items } from './items';
import { ItemsService } from './items.service';
import { HistoryService} from './history.service';


@Component({
    selector: 'requests-app',
    templateUrl: `app/app.component.html`,
    styleUrls: [`app/app.component.css`]
})

export class AppComponent {

  items: Items[];

  constructor(private itemsService: ItemsService){}

  ngOnInit() {
  this.itemsService.getItems()
  .subscribe(items => this.items = items);
  }

  products = {
    "Avalanche Online": "https://start.avalancheonline.ru/static/img/ao_logo.png",
    "Лавина Пульс": "https://start.avalancheonline.ru/static/img/logo-LP.jpg"
  }

  statuses = {
    "red": "status-red",
    "yellow": "status-yellow",
    "green": "status-green"
  }

  letters = {
     "false": "",
     "true": "active"
  }

}
