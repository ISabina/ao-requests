import { Injectable } from '@angular/core';
import { Http, Response, Headers, RequestOptions } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import 'rxjs/Rx';

@Injectable()
export class HistoryService{

    constructor(private http: Http){ }

    getHistory(orderId){
        return this.http.get('https://start.avalancheonline.ru/api/v2/landing/orders/'+orderId+'/history/?token=ea685397-d24c-4416-86ff-71277c1061ef')
        .map(response => <History[]>response.json().items)
    }


    getComments(orderId){
        return this.http.get('https://start.avalancheonline.ru/api/v2/landing/orders/'+orderId+'/comments/?token=ea685397-d24c-4416-86ff-71277c1061ef')
        .map(response => <History[]>response.json().items)
    }
}