export class History{
   username: string;
  user_id: number;
  order_id: number;
  created_at: date;
  updated_at: date;
  diff: string;
  id: number;
}