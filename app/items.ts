export class Items {
comment: string;
status: string;
product: string;
mail_sent: boolean;
user_id: number;
name: string;
mails: string;
product_name: string;
created_at: date;
updated_at: date;
id: number;
comments_count: number;
status_label: string;
mails_count: number;
organization: string;
project_id: number;
work_field: string;
email: string;
work_field_label: string;
}